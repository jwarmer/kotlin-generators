package model

import transform.Rule
import transformed.transform.Transformer

class Model2Database : Rule<Model, Database>(){

    override fun check(src: Model): Boolean {
        return true;
    }

    override fun build(s: Model, tr: Transformer): Database {
        val db = database {
            name = s.name
            tables = tr.transformAll2(s.elements)
        }
//        this.setProperties(db, s, tr);
        return db;
    }

    override fun setProperties(t: Database, s: Model, tr: Transformer)  {
        s.elements.forEach {
            val tbl: Any? = tr.transform(it)
            if( tbl != null){
                t.tables.add(tbl as Table)
            }
        }
    }
}

class Element2Table : Rule<Element, Table>(){

    override fun check(src: Element): Boolean {
        return true;
    }

    override fun build(s: Element, tr: Transformer): Table {
        val tbl = table {
            name = s.name
            columns = tr.transformAll2(s.properties)
        }
//        this.setProperties(tbl, s, tr);
        return tbl;
    }

    override fun setProperties(t: Table, s: Element, tr: Transformer)  {
        s.properties.forEach {
            val tbl: Any? = tr.transform(it)
            if( tbl != null){
                t.columns.add(tbl as Column)
            }
        }
    }
}

class Property2Column : Rule<Property, Column>(){

    override fun check(src: Property): Boolean {
        return true;
    }

    override fun build(s: Property, tr: Transformer): Column {
        return column {
            name = s.name
        }
    }

    override fun setProperties(t: Column, s: Property, tr: Transformer)  {

    }
}


