package transformed.transform

import transform.Rule
import java.lang.reflect.Modifier
import java.util.HashMap
import java.util.Vector

public interface Transformer {
    fun transform(source: Any): Any?
    fun <S, T> transform2(source: S): T?
    fun transformAll(sourceObjects: List<Any>): MutableList<Any>
    fun <S, T> transformAll2(sourceObjects: List<S>): MutableList<T>
    fun <S, T> transform(ruleType: Class<out Rule<S, T>>, source: S): T?
    fun <S, T> transformAll(ruleType: Class<out Rule<S, T>>, source: List<S>): List<T>
}

public class SimpleTransformerImpl() : Transformer {
    internal var ruleTypes: MutableList<Class<out Rule<*, *>>> = Vector()

    internal var mappings: MutableMap<Class<out Rule<*, *>>, Map<Any, Any>> = HashMap()

    internal fun <S, T> getRuleMappings(rule: Class<out Rule<S, T>>): MutableMap<S, T> {
        var ruleMappings: MutableMap<S, T>? = null
        var x: Any? = mappings[rule]
        if( x != null ){
            ruleMappings = x as MutableMap<S, T>
        }
        if (ruleMappings == null) {
            ruleMappings = HashMap<S, T>()
            mappings[rule] = ruleMappings as Map<Any, Any>
        }
        return ruleMappings
    }

    internal fun <S, T> recordMaping(rule: Class<out Rule<S, T>>, source: S, target: T) {
        getRuleMappings<S, T>(rule)[source] = target
    }

    internal fun <S, T> getExistingTargetFor(rule: Class<out Rule<S, T>>, source: S): T? {
//        return getRuleMappings<Any, Any>(rule).get(source)
        var x = getRuleMappings<S, T>(rule)
        return x.get(source)
    }

    internal fun <S, T> applyRule(r: Rule<S, T>, source: S): T? {
        val ruleType = r.javaClass as Class<out Rule<S, T>>
        var target: T? = getExistingTargetFor(ruleType, source)
        if (target == null) {
            target = r.build(source, this)
            recordMaping(ruleType, source, target)
            r.setProperties(target, source, this)
        }

        return target
    }

    fun getRuleTypes(): MutableList<Class<out Rule<*, *>>> {
        return this.ruleTypes
    }

    fun <S, T> addRuleType(ruleType: Class<out Rule<S, T>>) {
        getRuleTypes().add(ruleType)
    }

    fun <S, T> getRules(ruleType: Class<out Rule<S, T>>): List<Rule<S, T>> {
        val rules = Vector<Rule<S, T>>()
        for (rt in getRuleTypes()) {
            if (ruleType.isAssignableFrom(rt)) {
                if (!Modifier.isAbstract(rt.modifiers)) {
                    try {
                        rules.add(rt.newInstance() as Rule<S, T>)
                    } catch (e: InstantiationException) {
                        throw RuntimeException(e)
                    } catch (e: IllegalAccessException) {
                        throw RuntimeException(e)
                    }

                }
            }
        }
        return rules
    }

    override fun <S, T> transform(ruleType: Class<out Rule<S, T>>, source: S): T? {
        val rules = getRules<S, T>(ruleType)
        if (rules.isEmpty()) {
            throw RuntimeException(source.toString())
        } else {
            var classCastsExcs = 0
            for (rule in rules) {
                var b: Boolean? = false
                try {
                    b = rule.check(source)
                } catch (e: ClassCastException) {
                    classCastsExcs++
                }

                if (b!!) {
                    return applyRule(rule, source)
                }
            }
            if (classCastsExcs == rules.size) {
                // No rule can be found to be Applied
                // This is an error
                throw RuntimeException("No rule can be found to be Applied: " + source.toString())
            }
        }
        throw RuntimeException(source.toString())
//        return null
    }

    override fun <S, T> transformAll(ruleType: Class<out Rule<S, T>>, source: List<S>): MutableList<T> {
        val targets = Vector<T>()
        for (s in source) {
            val o = transform(ruleType, s)
            // Please note that the element generated may be null.
            targets.add(o)
        }
        return targets
    }

    override fun transform(source: Any): Any? {
        return transform(Rule::class.java as Class<out Rule<Any, Any>>, source)
    }

    override fun <S, T> transform2(source: S): T? {
        return transform<S, T>(Rule::class.java as Class<out Rule<S, T>>, source);
    }

    override fun transformAll(sourceObjects: List<Any>): MutableList<Any> {
        return transformAll(Rule::class.java as Class<out Rule<Any, Any>>, sourceObjects)
    }
    override fun <S, T> transformAll2(sourceObjects: List<S>): MutableList<T> {
        return transformAll(Rule::class.java as Class<out Rule<S, T>>, sourceObjects)
   }

}
