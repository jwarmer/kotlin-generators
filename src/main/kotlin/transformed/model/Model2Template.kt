package model

// See https://medium.com/@MaxMello/most-elegant-way-of-using-gson-kotlin-with-default-values-and-null-safety-b6216ac5328c
// See http://kotlination.com/kotlin/kotlin-convert-object-to-from-json-gson
//import com.google.gson.GsonBuilder

// from https://dzone.com/articles/cloud-foundry-application-manifest-using-kotlin-ds

var properties: MutableList<Property> = mutableListOf()

fun model(init: Model.() -> Unit): Model {
    val model: Model = Model();
    model.init();
    return model;
}

fun main(args: Array<String>) {

    val myModel = model {
        name = "MyModel"
        element {
            name = "element1";
        }
        element {
            name = "Person";
            property {
                name = "name"
                type = PropertyType.TString
            }
            property {
                name = "size"
                type = PropertyType.TNumber
            }
            properties.forEach { p: Property ->
                property {
                    name = p.name
                    type = p.type
                }
            }
        }
    }


    print(myModel.elements);
}
