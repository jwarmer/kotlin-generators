package transform

import transformed.transform.Transformer

abstract class Rule<SRC, TARGET> {
    abstract fun check(src: SRC): Boolean;

    abstract fun build(s: SRC, t: Transformer): TARGET;

    abstract fun setProperties(t: TARGET, s: SRC, tr: Transformer);
}
