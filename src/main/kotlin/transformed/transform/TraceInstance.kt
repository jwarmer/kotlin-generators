package transformed.transform

import transform.Rule

/**
 *
 *  Represents a TraceInstance, i.e. holds the runtime mapping information
 * between a source an a target object. In essence this is a triple
 * that holds for each rule that has been executed the
 * `(source object, target object, rule)` information.
 *
 * @param <S> The type of the source object.
 * @param <T> The type of the target object.
 *
 * @author Kyriakos Anastasakis
 * @since 0.2
</T></S> */
interface TraceInstance<S, T> {

    /**
     *
     * @return The [Rule] responsible for the mapping
     */
    val rule: Rule<S, T>

    /**
     *
     * @return The source object of the mapping
     */
    val source: S

    /**
     *
     * @return The target object of the mapping
     */
    val target: T

}
