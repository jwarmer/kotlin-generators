package model

class Database {
    var name: String = "";
    var tables: MutableList<Table> = mutableListOf()

    fun table(init: Table.() -> Unit): Table {
        val e = Table();
        e.init();
        this.tables.add(e);
        return e;
    }

}

class Table {
    var name: String = "";
    var columns: MutableList<Column> = mutableListOf()

    fun column(init: Column.() -> Unit): Column {
        val e = Column();
        e.init();
        this.columns.add(e);
        return e;
    }
}

class Column {
    var name: String = "";
    var type: DbType = DbType.Varchar;
}

enum class DbType {
    Varchar,
    Number,
    Boolean,
    Date
}

// Functions to support internal Kotlin DSL sytax

fun database(init: Database.() -> Unit): Database {
    val model: Database = Database();
    model.init();
    return model;
}

fun table(init: Table.() -> Unit): Table {
    val model: Table = Table();
    model.init();
    return model;
}

fun column(init: Column.() -> Unit): Column {
    val model: Column = Column();
    model.init();
    return model;
}

