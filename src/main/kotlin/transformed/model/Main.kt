package transformed.model

import com.google.gson.GsonBuilder
import model.*
import transformed.transform.SimpleTransformerImpl
import transformed.transform.Transformer

val gson = GsonBuilder().setPrettyPrinting().create() // for pretty print feature
var myModel: Model = Model();

fun main(args: Array<String>) {
    println("Hello");
    init();
    println(gson.toJson(myModel))

    val tr = SimpleTransformerImpl();
    tr.addRuleType(Model2Database::class.java)
    tr.addRuleType(Element2Table::class.java)
    tr.addRuleType(Property2Column::class.java)

    println(gson.toJson(Model2Database().build(myModel, tr)))
}

fun init() {
    myModel = model {
        name = "MyModel"
        element {
            name = "element1";
        }
        element {
            name = "Person";
            property {
                name = "name"
                type = PropertyType.TString
            }
            property {
                name = "size"
                type = PropertyType.TNumber
            }
        }
    }
}