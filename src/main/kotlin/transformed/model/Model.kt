package model

class Model {
    var name: String = "dummy"
    var version: String = "0.0"

    var elements: MutableList<Element> = mutableListOf()

    fun element(init: Element.() -> Unit): Element {
        val e = Element();
        e.init();
        this.elements.add(e);
        return e;
    }
}

class Element {
    var name: String = "dummy"

    var properties: MutableList<Property> = mutableListOf()

    fun property(init: Property.() -> Unit): Property {
        val e = Property();
        e.init()
        this.properties.add(e);
        return e;
    }

}

class Property {
    var name: String = "dummy"
    var type: PropertyType = PropertyType.TString
}

enum class PropertyType {
    TDate,
    TString,
    TNumber,
    TCurrency
}