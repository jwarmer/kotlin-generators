package transformed.transform

import transform.Rule

/**
 *
 * An interface that exposes the methods that can be used for tracing.
 *
 *
 * @author Kyriakos Anastasakis
 * @since 0.2
 *
 * @author John Saxon
 * @since 0.2.1
 *
 * @see TraceImpl
 *
 * @see TraceInstanceImpl
 */
interface ITrace {
    val traceInstances: List<TraceInstance<*, *>>
    var isEnabled: Boolean

    /**
     * Records a mapping between a source and a target element for a given rule.
     */
    fun <S, T> recordMapping(source: S, target: T, r: Rule<S, T>): TraceInstance<S, T>

    fun <S, T> recordMapping(ti: TraceInstance<S, T>)


    /**
     * Corresponds to the **resolveone** of the QVT specification
     */
    fun <S, T> resolveone(src: S): T

    /**
     * Corresponds to the **resolveone** of the QVT specification
     * where a condition of a class type is passed as a parameter
     */
    fun <S, T> resolveone(src: S, targetType: Class<T>): T

    /**
     * Corresponds to the **resolveone** of the QVT specification
     * applied to a list of source objects
     */
    fun <S, T> resolveone(srclst: List<S>): T

    /**
     * Corresponds to the **resolveone** of the QVT specification
     * applied to a list of source objects and requiring a condition
     */
    fun <S, T> resolveone(srclst: List<S>, targetType: Class<T>): T

    /**
     * Corresponds to the **resolve** of the QVT specification
     */
    fun resolve(src: Any): List<Any>

    /**
     * Corresponds to the **resolveone** of the QVT specification
     * applied to a collection of source objects.
     */
    fun resolve(srclist: List<Any>): List<Any>

    /**
     * Corresponds to the **resolve** of the QVT specification
     * where an extra condition is used to constraint the type
     * of target objects returned
     */
    fun <S, T> resolve(src: S, targetType: Class<T>): List<T>

    /**
     * Corresponds to the **resolve** of the QVT specification
     * applied to a collection of source objects and
     * an extra condition is used to constraint the type
     * of target objects returned
     */
    fun <S, T> resolve(srclist: List<S>, targetType: Class<T>): List<T>


    /**
     *
     *
     * Returns a List of the target objects of a specific kinds (*targetType*)
     *
     *  For example if used like: <br></br>
     * `
     * trace.resolve(Table.class)
    ` *
     * <br></br>
     * it will return target Table objects.
     *
     *
     * @param <T> The type of returned objects
     * @param targetType The class type
     * @return A List of target Objects that conform to the class type passed as a parameter
    </T> */
    fun <T> resolve(targetType: Class<out T>): List<T>

    /**
     *
     * Returns a List of the source object of a specific kind (*sourceType*)
     *
     *
     *  For example if used like: <br></br>
     * `
     * trace.resolvein(Person.class)
    ` *
     * <br></br>
     * it will return source Person objects.
     *
     *
     * @param <S> The type of the source object
     * @param <T> The type of the target objects
     * @param targetList The list of target objects to match
     * @param sourceType Only find traces of sourceType and match them against the targetList. Can be null.
     * @return A List of source elements that map to the target
    </T></S> */
    fun <S, T> resolvein(targetList: List<T>, sourceType: Class<S>): List<S>
}
